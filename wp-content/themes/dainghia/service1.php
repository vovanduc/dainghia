<?php
/* 
  Template Name: Service
*/

get_header();
?>
<div class="inner">
    <div class="content">
        <h3 class="h3-content"><?php echo get_the_title() ?></h3>
        <ul class="activity ct-service page-ct">
            <?php
            $arg = array( 'post_type' => 'service');
            $my_query = new WP_Query( $arg );
            if ( $my_query->have_posts() ) { 
                while ( $my_query->have_posts() ) { 
                    $my_query->the_post();
                    ?>
                        <li>
                            <?php 
                                $thumbnail_id = get_post_thumbnail_id();
                                $thumbnail_url = wp_get_attachment_image_src( $thumbnail_id);
                            ?>
                            <a href="<?php echo the_permalink() ?>">
                                <img src="<?php echo $thumbnail_url[0] ?>" height="160" width="280" alt="<?php echo get_the_title() ?>">
                                <p class="title"><?php echo get_the_title() ?></p>
                                <p><?php echo wp_trim_words(get_the_content(),10,'...') ?></p>
                            </a>
                        </li>
                    <?php
                }
                
            }
            wp_reset_postdata();
            ?>
        </ul>
    </div>
</div>  
<?php


get_footer();