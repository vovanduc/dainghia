<?php 
get_header();
?>
<div class="inner">
      <div class="content content-other ct-intro">
          <h3 class="h3-content">Tại sao lại chọn An Đại Nghĩa?</h3>
		<?php
		if(have_posts()){
			while (have_posts()) {
				the_post();

				echo do_shortcode( get_the_content() );

			}
		}
		?>
      </div>
      <div class="sidebar">
          <ul class="sb-menu">
            <li><a href="">Lịch sử hình thành</a></li>
            <li><a href="">Tại sao lại chọn An Đại Nghĩa?</a></li>
            <li><a href="">Sơ đồ tổ chức</a></li>
            <li><a href="">Tầm nhìn, sứ mệnh</a></li>
            <li><a href="">Tiêu chí kinh doanh</a></li>
          </ul>
          <h3 class="sb-h3-dip"><span>Giấy chứng nhận</span></h3>
          <ul class="sb-diploma">
            <li><img src="images/img-dip-1.png" height="122" width="103" alt=""></li>
            <li><img src="images/img-dip-2.png" height="122" width="103" alt=""></li>
          </ul>
          <h3 class="sb-h3-bag"><span>Hồ sơ năng lực</span></h3>
          <a href="#"><img src="images/bt-pdf-1.jpg" height="54" width="273"></a>
      </div>
</div>
<?php
get_footer(); 
?>
