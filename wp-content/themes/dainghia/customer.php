<?php
/* 
  Template Name: Customer
*/

get_header();
?>
<div class="inner">
    <div class="content">
        <h3 class="h3-content"><?php echo get_the_title() ?></h3>
        <ul class="ct-customer">
            
            <?php
             $args = array(
            'post_type' => 'customer'
            );
            $my_query = new WP_Query( $args );
            if($my_query->have_posts()){
                while ($my_query->have_posts()) {
                    $my_query->the_post();
                    ?>
                    <li>
                          <?php 
                            $thumbnail_id = get_post_thumbnail_id();
                            $thumbnail_url = wp_get_attachment_image_src( $thumbnail_id);
                            $image = get_field('customer_images');
                            echo '<img src="'.$image['url'].'" copyright="yes" alt="'.$image['alt'].'">';
                          ?>
                          <p><?php echo get_the_title(); ?></p>
                    </li>
                    <?php
                }
            }
            ?>
        </ul>
    </div>
</div>  
<?php
get_footer();