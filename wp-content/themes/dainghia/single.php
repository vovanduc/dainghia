<?php 
get_header();
?>
<div class="inner">
      <div class="content content-other ct-intro">
          <h3 class="h3-content"><?php echo get_the_title() ?></h3>
		<?php
		if(have_posts()){
			while (have_posts()) {
				the_post();

				the_content();

			}
		}
		?>
      </div>
      <?php 
      get_sidebar();
      ?>
    </div>
<?php
get_footer(); 
?>
