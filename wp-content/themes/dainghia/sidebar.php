<?php
$lang = get_bloginfo("language");
?>
<div class="sidebar">
  <ul class="sb-menu">
    <?php
    $args = array(
  'type'                     => 'intro1',
  'orderby'                  => 'name',
  'order'                    => 'ASC',
  'taxonomy'                 => 'category',
  'hide_empty'               => 0,
  ); 
    $categories = get_categories($args);
    foreach ($categories as $key) {
      if(intval($key->term_id)==1){
        continue;
      }

    $args = array( 'post_type' => 'intro1' , 'posts_per_page' => 1 , 'category' => $key->term_id );
    $myposts = get_posts( $args );
    if(count($myposts)>0){
      foreach ( $myposts as $post ) : setup_postdata( $post );
        echo '<li><a href="'.get_the_permalink().'">'.$key->name.'</a></li>';
      endforeach; 
      wp_reset_postdata();
    }else{
      echo '<li><a href="#">'.$key->name.'</a></li>';
    }
    }
    ?>
  </ul>
  <h3 class="sb-h3-dip">
    <span>
      <?php
      if($lang==='vi'){
          echo 'Giấy chứng nhận';
      }else if($lang==='en-US'){
          echo 'Text';
      }
      ?>
    </span>
  </h3>
  <ul class="sb-diploma">
    <?php
      $n = 0;
      $arg = array( 'post_type' => 'certificate', 'post_per_page'=>2);
      $my_query = new WP_Query( $arg );
      if ( $my_query->have_posts() ) { 
        while ( $my_query->have_posts() ) { 
            $my_query->the_post();
            $n++;
            $image = get_field('certificate');
            if( !empty($image) ): 
              echo '<li><a class="fancybox-button" rel="fancybox-button" href="'.$image['url'].'" title="'.$image['alt'].'">
                <img src="'.$image['url'].'" alt="'.$image['alt'].'" />
              </a></li>';
            endif;
            if($n==2)break;
        }
        wp_reset_postdata();
      }
      ?>
  </ul>
  <h3 class="sb-h3-bag">
    <span>
      <?php
      if($lang==='vi'){
          echo 'Hồ sơ năng lực';
      }else if($lang==='en-US'){
          echo 'Text';
      }
      ?>
    </span>
  </h3>
  <?php
    $n1 = 0;
    $arg = array( 'post_type' => 'file','post_per_page'=>1);
    $my_query = new WP_Query( $arg );
    if ( $my_query->have_posts() ) { 
      while ( $my_query->have_posts() ) { 
          $my_query->the_post();
          $n1++;
          $file = get_field('file');
          if( $file ):
            echo '<a href="'.$file['url'].'"><img src="'.get_template_directory_uri().'/images/bt-pdf-1.jpg"></a>';
          endif;
          if($n1==1)break;
      }
      wp_reset_postdata();
    }
    ?>
  
</div>