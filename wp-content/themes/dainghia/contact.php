<?php
/* 
Template Name: contact 
*/
get_header();
?>
<div class="inner">
      <div class="content contact_wp">
          <h3 class="h3-content"><?php echo get_the_title() ?></h3>
      		<?php
      		if(have_posts()){
      			while (have_posts()) {
      				the_post();

      				echo do_shortcode( get_the_content() );

      			}
      		}
      		?>
      </div>
</div>
<?php
get_footer(); 
?>
