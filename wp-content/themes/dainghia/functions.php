<?php
$lang = get_bloginfo("language");

add_theme_support( 'post-thumbnails' );

function wptutsplus_register_theme_menu() {
    register_nav_menu( 'primary', 'Main Navigation Menu' );
    register_post_type(
        'post_type_name'
    ,   array (
            'can_export'          => TRUE
        ,   'exclude_from_search' => FALSE
        ,   'has_archive'         => TRUE
        ,   'hierarchical'        => TRUE
        ,   'label'               => 'Intro'
        ,   'menu_position'       => 5
        ,   'public'              => TRUE
        ,   'publicly_queryable'  => TRUE
        ,   'query_var'           => 'intro1'
        ,   'show_ui'             => TRUE
        ,   'show_in_menu'        => TRUE
        ,   'show_in_nav_menus'   => TRUE
        ,   'supports'            => array ( 'editor', 'title' )
        )
    );
}
add_action( 'init', 'wptutsplus_register_theme_menu' );

function contact_address($atts,$content="") {
	extract( shortcode_atts( array(
		'icon' => '1'
	), $atts));
	return "<li  class='ct-icon".$icon."'>".$content."</li>";
}
add_shortcode( 'txt_contact', 'contact_address' );

function wrapper_contact_address($atts, $content="") {
	return "<ul class='contact_txt'>".do_shortcode($content)."</ul>";
}
add_shortcode( 'wp_contact', 'wrapper_contact_address' );

function wp_contact_right($atts,$content="") {
	return "<div class='wp_contact_right'>".do_shortcode($content)."</div>";
}
add_shortcode( 'wp_contact_right', 'wp_contact_right' );

function googlemap($atts,$content="") {
	return '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3833.9774254959802!2d108.2109579!3d16.066661200000006!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3142184ae3d1c18f%3A0x5630ba92c23a86c8!2zMzI1IEjDuW5nIFbGsMahbmcsIFRoYW5oIEtow6osIMSQw6AgTuG6tW5nLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1443972586238" 
	width="415" height="450" frameborder="0" 
	style="border:0" allowfullscreen>
	</iframe>';
}
add_shortcode( 'map', 'googlemap' );

function wrapper_contact($atts,$content=null) {
	$str = do_shortcode($content);
	return "<div class='wp_ct_right'>".$str."</div>";
}
add_shortcode( 'wp_ct_right', 'wrapper_contact' );


function service($atts,$content=null) {
	$output = '<h3 class="h3-service">'.$str.'</h3>';
	$output .= '<ul class="ct-service">';
	  $arg = array( 'post_type' => 'service','post_per_page'=>4);
	  $my_query = new WP_Query( $arg );
	  if ( $my_query->have_posts() ) { 
	    while ( $my_query->have_posts() ) { 
	        $my_query->the_post();
	          $output .= '<li>';
	          $output .= '<a href="'.the_permalink().'">';
              $thumbnail_id = get_post_thumbnail_id();
              $thumbnail_url = wp_get_attachment_image_src( $thumbnail_id);
              $output .= '<img src="'.$thumbnail_url[0].'" alt="'.get_the_title().'">';
              $output .= '<p class="title">'.get_the_title().'</p>';
              $output .= '<p>'.wp_trim_words(get_the_content(),15,'...').'</p>';
	          $output .= '</a>';
	          $output .= '</li>';
	    }
	  }
	  wp_reset_postdata();
      $output .= '</ul>';
      return $output;
}
add_shortcode( 'service', 'service' );

function wrapper($atts,$content=null) {
	extract( shortcode_atts( array(
    'class' => ''
    ), $atts ) );
	$str = do_shortcode($content);
	return '<div class="'.$class.'">'.$str.'</div>';
}
add_shortcode( 'wrapper', 'wrapper' );

function choose_us($atts,$content=null) {
	$str = do_shortcode($content);
	$output .= '<div class="choose_us">';
    $output .= '<h3>'.$str.'</h3>';
    $output .= '<p>';
      $arg = array( 'post_type' => 'intro1', 'cat' => 5, 'posts_per_page' => 1 ) ;
      $query = new WP_Query( $arg );
      if ( $query->have_posts() ) {
        while ( $query->have_posts() ) {
          $query->the_post();
            $output .= '<a href="'.get_the_permalink().'">';
            $output .= get_the_content('');
            $output .= get_the_excerpt();
            $output .= '</a>';
        }
        
      }
      wp_reset_postdata();
    $output .= '</p>';
    $output .= '</div>';

    return $output;
}
add_shortcode( 'choose_us', 'choose_us' );

function the_news($atts,$content=null) {
	$str = do_shortcode($content);
	$output .= '<div class="the_news-index">';
    $output .= '<h3>'.$str.'</h3>';
    $output .= '<ul>';
    $arg = array( 'post_type' => 'thenews','post_per_page'=>3);
    $my_query = new WP_Query( $arg );
    if ( $my_query->have_posts() ) { 
        while ( $my_query->have_posts() ) { 
            $my_query->the_post();
              	$output .= '<li>';
                $output .= '<a href="'.the_permalink().'">';
	            $thumbnail_id = get_post_thumbnail_id();
	            $thumbnail_url = wp_get_attachment_image_src( $thumbnail_id);
	            $output .= '<img src="'.$thumbnail_url[0].'" alt="'.get_the_title().'">';
                $output .= '<p class="title">'.get_the_title().'</p>';
                $output .= '<p>'.wp_trim_words(get_the_content(),10,'...').'</p>';
                $output .= '</a>';
              	$output .= '</li>';
        }
        wp_reset_postdata();
      }
    $output .= '</ul>';
    $output .= '</div>';

    return $output;
}
add_shortcode( 'the_news', 'the_news' );

function certificate($atts,$content=null) {
	$str = do_shortcode($content);
	$output = '<h3>'.$str.'</h3>';
    $output .= '<ul>';
	$n1=0;
	$arg = array( 'post_type' => 'certificate','post_per_page'=>3);
	$my_query = new WP_Query( $arg );
	if ( $my_query->have_posts() ) { 
	    while ( $my_query->have_posts() ) { 
	        $my_query->the_post();
	        $n1++;
	        $image = get_field('certificate');
	        if( !empty($image) ): 
	          $output .= '<li><a class="fancybox-button" rel="fancybox-button" href="'.$image['url'].'" title="'.$image['alt'].'">
	            <img src="'.$image['url'].'" alt="'.$image['alt'].'" />
	          </a></li>';
	        endif;
	        if($n1==3)break;
	    }
	    wp_reset_postdata();
	  }
	  $output .= '</ul>';
	  return $output;
}
add_shortcode( 'certificate', 'certificate' );

function file_dn($atts,$content=null) {
	$str = do_shortcode($content);
	$output = '<h3>'.$str.'</h3>';
	$n = 0;
	$arg = array( 'post_type' => 'file','post_per_page'=>1);
	$my_query = new WP_Query( $arg );
	if ( $my_query->have_posts() ) { 
	    while ( $my_query->have_posts() ) { 
	        $my_query->the_post();
	        $n++;
	        $file = get_field('file');
	        if( $file ):
	          $output .= '<a href="'.$file['url'].'"><img src="'.get_template_directory_uri().'/images/bt-pdf.png" class="bt_index1"></a>';
	        endif;
	        if($n==1)break;
	    }
	    wp_reset_postdata();
	}
    return $output;
}
add_shortcode( 'file_dn', 'file_dn' );

function customer($atts,$content=null) {
	$str = do_shortcode($content);
	$output .= '<h3>'.$str.'</h3>';
    $output .= '<ul id="scroller">';
    $arg = array( 'post_type' => 'customer');
    $my_query = new WP_Query( $arg );
    if ( $my_query->have_posts() ) { 
        while ( $my_query->have_posts() ) { 
            $my_query->the_post();

            $image = get_field('customer_images');
            if( !empty($image) ): 

              $output .= '<li><img src="'.$image['url'].'" alt="'.$image['alt'].'" height="56" /></li>';

            endif;

        }
        wp_reset_postdata();
      }
    $output .= '</ul>
        <script type="text/javascript">
            $(document).ready(function() {
               $("#scroller").simplyScroll();

               $(".fancybox-button").fancybox({
                  prevEffect    : "none",
                  nextEffect    : "none",
                  closeBtn    : false,
                  helpers   : {
                    title : { type : "inside" },
                    buttons : {}
                  }
                });
            });
        </script>';
    return $output;
}
add_shortcode( 'customer', 'customer' );
