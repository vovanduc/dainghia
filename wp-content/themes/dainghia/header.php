<?php
$lang = get_bloginfo("language");
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">

<head>
    <meta content="" name="keywords">
    <meta content="" name="description">
    <meta content="" name="copyright">
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta content="text/css" http-equiv="Content-Style-Type">
    <meta content="text/javascript" http-equiv="Content-Script-Type">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title></title>
    <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/library.meanmenu.css">
    <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/jqx-media-slider.min.css">
    <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/jquery.simplyscroll.css">
    <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/js/fancybox/jquery.fancybox.css">
    <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/style.css">
    <script src="<?php echo get_template_directory_uri() ?>/js/jquery-1.9.1.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/js/jquery.simplyscroll.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/js/fancybox/jquery.fancybox.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/js/scoll.js"></script>
    <?php 
    wp_head();
    ?>
</head>

<body <?php body_class(); ?>>
    <div class="header">
        <div class="inner">
            <a class="logo_top" href="<?php bloginfo('home') ?>">
                <img alt="" class="logo_top" src="<?php echo get_template_directory_uri() ?>/images/logo.png" />
            </a>
            <h1>
            <?php
            if($lang==='vi'){
                echo '“Đạo Đức - Nghiệp Vụ - Trung Thành - Trách Nhiệm”';
            }else if($lang==='en-US'){
                echo '“text - text - text - text”';
            }
            ?>
            </h1>
            <div class="header_right">
                <div class="language">
                  <a href="<?php echo site_url() ?>/vi/"><img src="<?php echo get_template_directory_uri() ?>/images/hd_lang_vn.png"></a>
                  <a href="<?php echo site_url() ?>/en/"><img src="<?php echo get_template_directory_uri() ?>/images/hd_lang_en.png"></a>
                </div>
                <img src="<?php echo get_template_directory_uri() ?>/images/hd_phone.png">
                <?php
                if($lang==='vi'){
                    echo '<p>đường dây nóng</p>';
                }else if($lang==='en-US'){
                    echo '<p>text</p>';
                }
                ?>

                <p class="no-phone">0511 5 644 555</p>
            </div>
        </div>
        <!--inner-->
    </div>
    <!--header-->
    <?php
    $defaults = array(
        'container'       => 'div',
        'container_class' => 'menu',
        'container_id'    => 'globalNavi',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
    );
    echo '<div class="nav">';
    wp_nav_menu( $defaults );
    echo '</div>';