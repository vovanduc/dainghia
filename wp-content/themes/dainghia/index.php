    <?php
    get_header();
    ?>
    <div class="banner">
      <?php echo do_shortcode("[espro-slider id=51]"); ?>
    </div>
     <div class="content">
      <div class="inner">
          <h3 class="h3-service">
            <?php
            if($lang==='vi'){
                echo '<span>Dịch vụ</span> của chúng tôi';
            }else if($lang==='en-US'){
                echo '<span>Text</span> của chúng tôi';
            }
            ?>
          </h3>
          <ul class="ct-service">
          <?php
          $arg = array( 'post_type' => 'service','post_per_page'=>4);
          $my_query = new WP_Query( $arg );
          if ( $my_query->have_posts() ) { 
            while ( $my_query->have_posts() ) { 
                $my_query->the_post();
                ?>
                  <li>
                    <a href="<?php echo the_permalink() ?>">
                      <?php 
                          $thumbnail_id = get_post_thumbnail_id();
                          $thumbnail_url = wp_get_attachment_image_src( $thumbnail_id);
                          echo '<img src="'.$thumbnail_url[0].'" alt="'.get_the_title().'">';
                      ?>
                      <p class="title"><?php echo __(get_the_title()) ?></p>
                      <p><?php echo wp_trim_words(get_the_content(),15,'...') ?></p>
                    </a>
                  </li>
                <?php
            }
          }
          wp_reset_postdata();
          ?>
          </ul>
          <div class="intro">
            <div class="choose_us">
              <h3>
              <?php
              if($lang==='vi'){
                  echo '<span>Tại sao <span>chọn An Đại Nghĩa?</span></span>';
              }else if($lang==='en-US'){
                  echo '<span>Why <span>chọn An Đại Nghĩa?</span></span>';
              }
              ?>
              </h3>
              <p>
              <?php
              $arg = array( 'post_type' => 'intro1', 'cat' => 5, 'posts_per_page' => 1 ) ;
              $query = new WP_Query( $arg );
              if ( $query->have_posts() ) {
                while ( $query->have_posts() ) {
                  $query->the_post();
                    //echo '<a href="'.get_the_permalink().'">';
                    the_content('');
                    the_excerpt();
                    //echo '</a>';
                }
                
              }
              wp_reset_postdata();
              ?>
              </p>
            </div>
            <div class="the_news-index">
              <h3>
                <span>
                  <?php
                  if($lang==='vi'){
                      echo 'Tin tức';
                  }else if($lang==='en-US'){
                      echo 'The news';
                  }
                  ?>
                </span>
              </h3>
              <ul>
              <?php
              $arg = array( 'post_type' => 'thenews','post_per_page'=>3);
              $my_query = new WP_Query( $arg );
              if ( $my_query->have_posts() ) { 
                while ( $my_query->have_posts() ) { 
                    $my_query->the_post();
                    ?>
                      <li>
                        <a href="<?php echo the_permalink() ?>">
                          <?php 
                              $thumbnail_id = get_post_thumbnail_id();
                              $thumbnail_url = wp_get_attachment_image_src( $thumbnail_id);
                              echo '<img src="'.$thumbnail_url[0].'" alt="'.get_the_title().'">';
                          ?>
                          <p class="title"><?php echo get_the_title() ?></p>
                          <p><?php echo wp_trim_words(get_the_content(),10,'...') ?></p>
                        </a>
                      </li>
                    <?php
                }
                wp_reset_postdata();
              }
              ?>
              </ul>
            </div>
            <div class="diploma">
              <h3>
                <span>
                  <?php
                  if($lang==='vi'){
                      echo 'Giấy chứng nhận';
                  }else if($lang==='en-US'){
                      echo 'text';
                  }
                  ?>
                </span>
              </h3>
              <ul>
              <?php
              $n1=0;
              $arg = array( 'post_type' => 'certificate','post_per_page'=>3);
              $my_query = new WP_Query( $arg );
              if ( $my_query->have_posts() ) { 
                while ( $my_query->have_posts() ) { 
                    $my_query->the_post();
                    $n1++;
                    $image = get_field('certificate');
                    if( !empty($image) ): 
                      echo '<li><a class="fancybox-button" rel="fancybox-button" href="'.$image['url'].'" title="'.$image['alt'].'">
                        <img src="'.$image['url'].'" alt="'.$image['alt'].'" />
                      </a></li>';
                    endif;
                    if($n1==3)break;
                }
                wp_reset_postdata();
              }
              ?>
              </ul>
              <h3>
                <span class="bag">
                  <?php
                  if($lang==='vi'){
                      echo 'Hồ sơ năng lực';
                  }else if($lang==='en-US'){
                      echo 'text';
                  }
                  ?>
                </span>
              </h3>
              <?php
              $n = 0;
              $arg = array( 'post_type' => 'file','post_per_page'=>1);
              $my_query = new WP_Query( $arg );
              if ( $my_query->have_posts() ) { 
                while ( $my_query->have_posts() ) { 
                    $my_query->the_post();
                    $n++;
                    $file = get_field('file');
                    if( $file ):
                      echo '<a href="'.$file['url'].'"><img src="'.get_template_directory_uri().'/images/bt-pdf.png" class="bt_index1"></a>';
                    endif;
                    if($n==1)break;
                }
                wp_reset_postdata();
              }
              ?>
              
            </div>
          </div>
      </div>
    </div>
    <div class="customer">
      <div class="inner">
        <h3>
          <span>
            <?php
            if($lang==='vi'){
                echo 'Khách hàng của chúng tôi';
            }else if($lang==='en-US'){
                echo 'Customer';
            }
            ?>
          </span>
        </h3>
        <ul id="scroller">
         <?php
              $arg = array( 'post_type' => 'customer');
              $my_query = new WP_Query( $arg );
              if ( $my_query->have_posts() ) { 
                while ( $my_query->have_posts() ) { 
                    $my_query->the_post();

                    $image = get_field('customer_images');
                    if( !empty($image) ): 

                      echo '<li><img src="'.$image['url'].'" alt="'.$image['alt'].'" height="56" /></li>';

                    endif;

                }
                wp_reset_postdata();
              }
              ?>
        </ul>
        <script type="text/javascript">
            $(document).ready(function() {
               $("#scroller").simplyScroll();

               $(".fancybox-button").fancybox({
                  prevEffect    : 'none',
                  nextEffect    : 'none',
                  closeBtn    : false,
                  helpers   : {
                    title : { type : 'inside' },
                    buttons : {}
                  }
                });
            });
        </script>
      </div>
    </div>
    <!--inner-->
    <?php
    get_footer();
    ?>