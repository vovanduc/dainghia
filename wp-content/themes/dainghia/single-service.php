<?php
get_header();
?>
    <div class="inner">
      <div class="content ct-left ct-intro">
          <h3 class="h3-content"><?php echo get_the_title() ?></h3>
          <?php
          if(have_posts()){
            while (have_posts()) {
              the_post();

              the_content();
            }
          }
          ?>
      </div>
      <div class="sidebar sb-right">
          <h3>
          <?php
          if($lang==='vi'){
              echo 'DỊCH VỤ';
          }else if($lang==='en-US'){
              echo 'SERVICE';
          }
          ?>
          </h3>
          <?php
          echo '<ul>';
          $arg = array( 'post_type' => 'service','post_per_page'=>6);
          $my_query = new WP_Query( $arg );
          if ( $my_query->have_posts() ) { 
              while ( $my_query->have_posts() ) { 
                  $my_query->the_post();
                  ?>
                    <li>
                      <a href="<?php echo the_permalink() ?>">
                          <?php 
                              $thumbnail_id = get_post_thumbnail_id();
                              $thumbnail_url = wp_get_attachment_image_src( $thumbnail_id);
                              echo '<img src="'.$thumbnail_url[0].'" copyright="yes" alt="'.get_the_title().'">';
                          ?>
                          <p class="sb-title"><?php echo get_the_title() ?></p>
                          <p><?php echo wp_trim_words(get_the_content(),10,'...') ?></p>
                      </a>
                    </li>
                  <?php
              }
          }
          echo '</ul>';
          ?>
      </div>
    </div>
<?php
get_footer();
