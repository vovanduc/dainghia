��    9      �  O   �      �  }  �     g     �  
   �     �  Z   �  
                  $     0     7     G     W  2   Z  �   �  1   g  6   �  =   �     	     	     .	     K	  
   ]	     h	     x	     �	     �	     �	     �	     �	     �	  	   �	     �	     
     $
     ;
     G
     S
     `
     h
  I   t
  ?   �
  O   �
     N     Z     i     �  [   �  .   �       B   $  V   g  $   �     �  $     �  '  �  �     P     k  
   ~  
   �  N   �     �     �                 
   '  
   2     =  6   @  �   w  8   V  6   �  C   �     
          *     F     V     i  '   �     �     �  	   �     �     �  
   �  	             2     C     Z     f     t     �     �  Q   �  E   �  B   -     p     }     �     �  g   �  <        [  K   h  e   �          (     /     '      )      $       !           5           6       0      
   1   3      +           %                       -                                          8   	          /      *   .                    ,         2   4       9   "      (              7   &   #                          <span class="ewic-introjs"><span class="ewic-intro-help"></span><a href="javascript:void(0);" onclick="startIntro();">Click Here to learn How to Create Slider</a></span><br /><br />Click <strong><i>Add Images</i></strong> button below and select an images that you want to show in your widget area.<br />Press <strong>Ctrl + click on each images</strong> to select multiple images. AMAZING Pro Version DEMO Add New Slider Auto Slide Check this out! Choose an entrance animation or effect and pass it to the slider. Default : easeInOutCubic Comparison Disable Easy Slider Edit Slider Enable Getting Started Global Settings ID If ON slides will automatically transition / play. If ON the plugin will automatically convert uppercase the first character of each word and replace - with spaces in a title. For example : ferrari-f12-berlinetta-interior will change to Ferrari F12 Berlinetta Interior If ON your image title will appear on the bottom. If ON, the lightbox slideshow will play automatically. If ON, your images will displayed in a lightbox when clicked. Image Slider Lightbox Slideshow Lightbox Slideshow Interval  Navigation Button New Slider No Slider Found No Slider Found In Trash Open an Images in a Lightbox Parent Slider Preview Search Slider Select/Upload Images Settings Shortcode Show Titles ( if Available ) Slider Delay Slider Effect / Easing Slider Item Slider Size Slider Style Sliders Smart Title The amount of time (in sec) between each auto transition. Default : 5 sec The amount of time (in sec) between each slide. Default : 5 sec There was an error retrieving the list from the server. Please try again later. Total Image UPGRADE to PRO Upgrade to Pro Version Use Timthumb Use this option to set Slider custom width and height. We recommend to set AUTO for height. Use this widget to display your images slider. View Slider You can set the navigation button style here. Default: Always Show You can set the slider style to fade, move vertical or horizontal. Default: Horizontal post type general nameImage Sliders post type general nameSliders post type singular nameImage Slider Plural-Forms: nplurals=1; plural=0;
Project-Id-Version: Image Slider (Lite)
POT-Creation-Date: 2015-10-24 05:56-0800
PO-Revision-Date: 2015-10-24 06:04-0800
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.5
X-Poedit-Basepath: ..
X-Poedit-WPHeader: easy-slider-widget-lite.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: 
Language: id
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 <span class="ewic-introjs"><span class="ewic-intro-help"></span><a href="javascript:void(0);" onclick="startIntro();">Klik di sini untuk belajar bagaimana membuat Slider</a></span><br><br>Klik tombol <strong><i>Tambahkan gambar</i></strong> di bawah ini dan pilih gambar yang ingin Anda Tampilkan.<br>Tekan <strong>Ctrl + klik pada setiap gambar</strong> untuk memilih beberapa gambar sekaligus. Versi Pro yang MENAKJUBKAN Tambah Slider Baru Auto Slide Lihat ini! Pilih masuk animasi atau efek dan terapkan  ke slider. Standar: easeInOutCubic Perbandingan Versi Nonaktifkan Easy Slider Mengedit Slider Aktifkan Perkenalan Pengaturan ID Jika ON slide akan secara otomatis transisi / bermain. Jika ON plugin akan secara otomatis mengkonversi huruf besar karakter pertama dari setiap kata dan mengganti - dengan spasi dalam judul. Sebagai contoh: ferrari-f12-Sport-interior akan berubah ke Ferrari F12 Sport Interior Jika pada gambar Anda judul akan muncul di bagian bawah. Jika, lightbox slideshow akan bermain secara otomatis. Jika ON, gambar Anda akan ditampilkan dalam lightbox ketika diklik. Image Slider Lightbox Slideshow Lightbox Slideshow Interval Tombol navigasi Tambah Slider Baru Slider tidak ditemukan Slider tidak ditemukan di tempat sampah Membuka gambar dalam Lightbox Induk Slider Pratinjau Cari Slider Pilih/Upload gambar Pengaturan Shortcode Tampilkan judul (jika tersedia) Penundaan slider Slider Effect / Easing Slider Item Ukuran slider Jenis Slider Slider Judul Pintar Jumlah waktu (dalam detik) antara masing-masing transisi otomatis. Standar: 5 sec Jumlah waktu (dalam detik) antara masing-masing slide. Standar: 5 sec Ada kesalahan mengambil daftar dari server. Harap coba lagi nanti. Total Gambar Upgrade ke PRO Upgrade ke Versi Pro Menggunakan Timthumb Gunakan pilihan ini untuk menetapkan Lebar dan tinggi Slider. Kami sarankan untuk mengatur AUTO height. Menggunakan widget ini untuk menampilkan slider gambar Anda. Lihat Slider Anda dapat mengatur gaya tombol navigasi di sini. Default: Selalu Tampilkan Anda dapat mengatur gaya slider untuk memudar, bergerak vertikal atau horisontal. Default: horisontal Image Sliders Slider Image Slider 